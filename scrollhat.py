import scrollphathd as sp
import numpy as np
import time

def scroll_init():
	sp.set_brightness(0.3)

def scroll_message(message):
	sp.clear()
	sp.rotate(180)
	length = sp.write_string(message)
	sp.show()
	length -= sp.width
	time.sleep(0.1)
	while length > 1:
		sp.scroll(1)
		sp.show()
		time.sleep(0.005)
		length -= 1
	time.sleep(0.1)

def scroll_Red():
	scroll_message("Red")


def scroll_Orange():
	scroll_message("Orange")


def scroll_Yellow():
	scroll_message("Yellow")


def scroll_Green():
	scroll_message("Green")


def scroll_Blue():
	scroll_message("Blue")


def scroll_Purple():
	scroll_message("Purple")


def scroll_Pink():
	scroll_message("Pink")


def scroll_White():
	scroll_message("White")


def scroll_Brown():
	scroll_message("Brown")


def scroll_Black():
	scroll_message("Black")

def scroll_Grey():
	scroll_message("Grey")

def scroll_Other():
	scroll_message("Other")


# visualisation code for glasses detector:

def scroll_glasses():
	sp.clear()
	frame = []

	for x in range(17):
		frame.append([2, x])

	for x in range(5):
		frame.append([1, x+2])
		frame.append([1, x+10])
		frame.append([6, x+10])
		frame.append([6, x+2])
		frame.append([3, x+6])

	for x in range(7):
		frame.append([5, x+1])
		frame.append([5, x+9])

	for x in range(2):
		frame.append([3, x+1])
		frame.append([4, x+1])
		frame.append([4, x+6])
		frame.append([3, x+14])
		frame.append([4, x+9])
		frame.append([4, x+14])

	for x in frame:
		sp.set_pixel(x[1], x[0], 0.8)
	sp.show()


def scroll_no_glasses(): #modify unicorn_no_glasses
	sp.clear()
	cross = []
	for x in range(17):
		cross.append([3, x])
	for x in cross:
		sp.set_pixel(x[1], x[0], 0.8)
	sp.show()

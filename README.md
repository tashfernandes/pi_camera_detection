Raspberry Pi Machine Learned Models over HTTP.

Instructions:

Note: To get the server working it needs to be connected to the Macquarie WiFi.

To run the server/predictor:
  $ ./run_http.sh &
  $ python3 predict.py --http


To run the client:
  $ python3 client.py [ip address of server]

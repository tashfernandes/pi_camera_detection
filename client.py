import requests
import argparse
import time
from unicorn import * #importing display functions
from scrollhat import *
from car_detector.unicornhat import *
from car_detector.scrollhat import *


def call_display_functions(result):
 unicorn_result = 'unicorn_'+result
 scroll_result = 'scroll_'+result

 if unicorn_result in globals():
    try:
        globals()[unicorn_result]() #calls corresponding function from unicorn/scroll file
    except IOError:
        print("IO Error caught...")
 if scroll_result in globals():
    try:
        globals()[scroll_result]() #calls corresponding function from unicorn/scroll file
    except IOError:
        print("IO error caught..")

def send_request_to_ip(ipaddr):
 r = requests.get(f"http://{ipaddr}/predict.txt")
 print(r.text)
 return r.text


if __name__ ==  "__main__":
  parser = argparse.ArgumentParser(description='Connect to remote PI running model to display results')
  parser.add_argument('ip', action="store", type=str)
  args = parser.parse_args()
  ipaddr = args.ip
 
  while True:
    result = send_request_to_ip(ipaddr)
    call_display_functions(result)
    time.sleep(0.5) 




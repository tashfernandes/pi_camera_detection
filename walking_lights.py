import scrollphathd as sp
import time

# set x and y to the toe pixel
def lifted(x, y):
	# foot
	sp.set_pixel(x, y, 1) # toe
	sp.fill(1, x = (x - 3), y = (y - 2), width = 3, height = 3)
	sp.set_pixel(x - 3, y, 0)

	# leg
	sp.set_pixel(x - 1, y - 6, 1)
	sp.set_pixel(x, y - 5, 1)
	sp.set_pixel(x - 1, y - 4, 1)
	sp.set_pixel(x - 2, y - 3, 1)

def flat(x, y):
	# foot
	sp.set_pixel(x, y, 1) #toe
	sp.fill(1, x = x - 3, y = (y - 2), width = 2, height = 3)
	sp.set_pixel(x - 1, y - 1, 1)
	sp.set_pixel(x - 1, y, 1)

	# leg
	sp.fill(1, x = (x - 3), y = (y - 6), width = 1, height = 4)

def legs_frame_1(left_x, left_y, right_x, right_y):
	# lifted left, flat right
	lifted(left_x, left_y)
	flat(right_x, right_y)

def legs_frame_2(left_x, left_y, right_x, right_y):
	# flat left, lifted right
	flat(left_x, left_y)
	lifted(right_x, right_y)

def animated():
	i = 9 # right toe x coordinate
	while i < 26:
		legs_frame_1(i - 6, 6, i, 6)
		sp.show()
		time.sleep(0.1)
		sp.clear()
		legs_frame_2((i - 5), 6, (i + 1), 6)
		sp.show()
		time.sleep(0.1)
		sp.clear()
		i += 2

def scroll_Using_laptop():
	sp.flip(x = True, y = True)
	animated()

def scroll_Not_using_laptop():
	sp.clear()
	sp.show()

if __name__ == '__main__':
	sp.flip(x = True, y = True)
	animated()

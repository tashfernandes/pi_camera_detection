import argparse
import subprocess
import tflite_runtime.interpreter as tflite
import numpy as np
from PIL import Image
import time
import glob
import picamera
from signal import pause

from gpiozero import Button
from unicorn import * #importing display functions
from scrollhat import *
from car_detector.unicornhat import *
from car_detector.scrollhat import *
from walking_lights import *

cam = picamera.PiCamera()
cam.resolution = (224,224)
cam.rotation = 180

def feed(lst_globs):
  if lst_globs == "":
    while True:
      cam.capture("images/camera-feed.jpg")
      yield "images/camera-feed.jpg"
  else:
    for img_glob in sources[src_i][1]:
      for img_path in glob.glob(img_glob):
        yield img_path

def update_model(i):
    global button_pressed, new_model, models

    idx = models[i][2].lower() + '_text'
    print("Calling ", idx)
    try: 
        unicorn_write(globals()[idx]())
    except:
        print("Function not implemented")

    try:
        scroll_write(models[i][2])
    except IOError:
        print("oops")

    new_model = i
    button_pressed = True

def run_modelA():
    # The sunglasses model
    update_model(0)

def run_modelB():
    # The car/person model
    update_model(1)

def run_modelX():
    # The liz model
    print("In Liz model")
    update_model(2)

def run_modelY():
    # The colour model
    print("In colour model")
    update_model(3)

## Main while loop
# Need to check whether the model has changed inside the loop

def change_model(model_i):
    print("Changing to model ", model_i)
    interpreter = tflite.Interpreter(models[model_i][1]+"/model.tflite")
    interpreter.allocate_tensors()
    
    labels = []
    with open(models[model_i][1]+"/labels.txt", "r") as f:
        labels = [line.strip() for line in f.readlines()]

    return (labels, interpreter)

# possibly check additional argument 'Which hat are you using?' - (unicorn, scroll, display, none)
argparser = argparse.ArgumentParser(description="Run a Model on a set of images or a camera feed and generate predictions.")
argparser.add_argument("--model", metavar="M", type=int, help="the index of the model to use.")
argparser.add_argument("--source", metavar="S", type=int, help="the index of the source to use.")
argparser.add_argument("--http", help="Run with http server", action="store_true")
args = argparser.parse_args()

models = [
          ("Custom: Is the person wearing sunglasses?", "models/sunglasses", "Glasses"),
          ("Custom: Car/person identifier", "car_detector/model", "Cars"),
          ("Custom: Liz identifier", "models/individual", "Liz"),
          ("Custom: Colour identifier", "models/colours_quant", "Colours"),
          ("Custom: Break reminder", "models/using_laptop", "Break"),
	  ("Custom: Colours bad", "models/colours_bad", "Colours"),
	  ("Custom: Colours new", "models/colours_new", "Colours")
	 ]

sources = [("Example Images",["example_pictures/*"
                              ]),
           ("Camera","")
          ]

http_running = False
http_file = None
if args.http:
    http_running = True
    http_file = './http/predict.txt'

button_a = Button(5)
button_b = Button(6)
button_x = Button(16)
button_y = Button(24)

button_a.when_pressed = run_modelA
button_b.when_pressed = run_modelB
button_x.when_pressed = run_modelX
button_y.when_pressed = run_modelY

if args.model is None:
  print("Which model would you like to run?")
  for i, mod in enumerate(models):
    print(f"({i}) {mod[0]}")
  model_i = int(input())
else:
  model_i = args.model

if args.source is None:
  print("Which source would you like predict from?")
  for i, src in enumerate(sources):
    print(f"({i}) {src[0]}")
  src_i = int(input())
else:
  src_i = args.source


(labels, interpreter) = change_model(model_i)
inputs = interpreter.get_input_details()[0];
outputs = interpreter.get_output_details()[0];
width  = inputs["shape"][2]
height = inputs["shape"][1]
dtype = inputs["dtype"]
scale, zero = outputs['quantization']
if (scale == 0):
  scale = 1

print(f"Predicting with model:  {models[model_i][0]}\n  * size: ({width}x{height})\n  * type: {dtype}\n  * quant: ({scale},{zero})")
print(f"Predicting from source: {sources[src_i][0]}")

button_pressed = False

try:
    scroll_init()
except IOError:
    print("IO error caught..")

for img_path in feed(sources[src_i][1]):
  img = Image.open(img_path).convert('RGB').resize((width, height))

  # we need another dimension
  input_data = np.expand_dims(img, axis=0).astype(dtype)

  # lets get to it
  interpreter.set_tensor(inputs["index"], input_data)
  interpreter.invoke()

  output_data = interpreter.get_tensor(outputs["index"]).squeeze()
  output_data = (scale*100) * (output_data - zero)

  ordered_indexes = np.flip(output_data.argsort())
  best_index = ordered_indexes[0]

  #(if hat attached) -> indent following code to be in if statement:
  result = labels[best_index]
  if best_index <= 9:
    result = result[2:] #substring to remove index prefix from label
  if best_index in range(10,99,1):
    result = result[3:]
  if best_index in range(100,999,1):
    result = result[4:]
  if best_index in range(1000,9999,1):
    result = result[5:]

  #if unicorn attached, elif scroll attached etc.
  #modify result string to append 'unicorn_' 'scroll_' etc.
  unicorn_result = 'unicorn_'+result
  scroll_result = 'scroll_'+result

  if button_pressed:
      time.sleep(2)

  if unicorn_result in globals():
    try:
        globals()[unicorn_result]() #calls corresponding function from unicorn/scroll file
    except IOError:
        print("IO Error caught...")
  if scroll_result in globals():
    try:
        globals()[scroll_result]() #calls corresponding function from unicorn/scroll file
    except IOError:
        print("IO error caught..")

  if http_running:
    with open(http_file, 'w') as f:
        f.write(result)

  print(f"  * {img_path} = {labels[best_index]} %0.0f%%" % output_data[best_index])
  print(f"                 {labels[ordered_indexes[1]]} %0.0f%%" % output_data[ordered_indexes[1]])

  if button_pressed:
      (labels, interpreter) = change_model(new_model)
      button_pressed = False
      inputs = interpreter.get_input_details()[0];
      outputs = interpreter.get_output_details()[0];
      width  = inputs["shape"][2]
      height = inputs["shape"][1]
      dtype = inputs["dtype"]
      scale, zero = outputs['quantization']
      print(f"Predicting with model:  {models[new_model][0]} ({width}x{height}) {dtype}")
      time.sleep(2)
    

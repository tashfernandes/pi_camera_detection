from unicornhatmini import UnicornHATMini
import numpy as np
import time

uh = UnicornHATMini()

def unicorn_glasses(): #modify unicorn_glasses
	uh.clear()
	frame = []

	for x in range(17):
		frame.append([2, x])

	for x in range(5):
		frame.append([1, x+2])
		frame.append([1, x+10])
		frame.append([6, x+10])
		frame.append([6, x+2])
		frame.append([3, x+6])

	for x in range(7):
		frame.append([5, x+1])
		frame.append([5, x+9])

	for x in range(2):
		frame.append([3, x+1])
		frame.append([4, x+1])
		frame.append([4, x+6])
		frame.append([3, x+14])
		frame.append([4, x+9])
		frame.append([4, x+14])

	for x in frame:
		uh.set_pixel(x[1], x[0], 0, 255, 0)
	uh.show()


def unicorn_no_glasses(): #modify unicorn_no_glasses
	uh.clear()
	cross = []
	for x in range(17):
		cross.append([3, x])
	for x in cross:
		uh.set_pixel(x[1], x[0], 255, 0, 0)
	uh.show()

def glasses_text():
    frame = [[0,0], [1,0], [2,0], [1,1], [2, 2], [2,1]]
    return frame

def unicorn_write(string):
    uh.clear()
    for x in string:
        uh.set_pixel(x[1], x[0], 0, 255, 0)
    uh.show()

def unicorn_Red():
	uh.clear()
	uh.set_all(255,0,0)
	uh.show()

def unicorn_Orange():
	uh.clear()
	uh.set_all(255,215,0)
	uh.show()

def unicorn_Yellow():
	uh.clear()
	uh.set_all(255,255,0)
	uh.show()

def unicorn_Green():
	uh.clear()
	uh.set_all(0,255,0)
	uh.show()

def unicorn_Blue():
	uh.clear()
	uh.set_all(0,0,255)
	uh.show()

def unicorn_Purple():
	uh.clear()
	uh.set_all(165,0,255)
	uh.show()

def unicorn_Pink():
	uh.clear()
	uh.set_all(255,53,184)
	uh.show()

def unicorn_White():
	uh.clear()
	uh.set_all(255,255,255)
	uh.show()

def unicorn_Brown():
	uh.clear()
	uh.set_all(101,67,33)
	uh.show()

def unicorn_Black():
	uh.clear()
	uh.set_all(0,0,0)
	uh.show()

def unicorn_Grey():
	uh.clear()
	uh.set_pixel(2, 2, 255, 255, 255)
	uh.show()

def unicorn_Other():
	uh.clear()
	line = []
	for x in range(6,10):
		line.append([x, 0, 6])
	for x in line:
		uh.set_pixel(x[0], x[1], 0, 0, 255)
	for x in line:
		uh.set_pixel(x[0], x[2], 0, 0, 255)
	line2 = []
	for x in range(7):
		line2.append([x, 6, 10])
	for x in line2:
		uh.set_pixel(x[1], x[0], 0, 0, 255)
	for x in line2:
		uh.set_pixel(x[2], x[0], 0, 0, 255)
	uh.show()

if __name__ == '__main__':
    unicorn_write("HI")
    while True:
        uh.show()

import argparse
import subprocess
import tflite_runtime.interpreter as tflite
import numpy as np
from PIL import Image
import time
import glob
import picamera
from scrollhat import *
from unicornhat import *
from gpiozero import Button
from signal import pause

def feed(lst_globs):
  global PLAY
  if lst_globs == "":
    while True:
      if PLAY:
        cam.capture("images/camera-feed.jpg")
        yield "images/camera-feed.jpg"
  else:
    for img_glob in sources[src_i][1]:
      for img_path in glob.glob(img_glob):
        yield img_path

def button_on():
    global PLAY
    PLAY = True
    scroll_on()
    time.sleep(1)

def button_off():
    global PLAY
    PLAY = False
    scroll_off()
    time.sleep(1)

if __name__ == '__main__':

    PLAY = False
    button_a = Button(5)
    button_b = Button(6)
    button_a.when_pressed = button_on
    button_b.when_pressed = button_off
    
    cam = picamera.PiCamera()
    cam.resolution = (224,224)
    cam.rotation = 180

    argparser = argparse.ArgumentParser(description="Run a Model on a set of images or a camera feed and generate predictions.")
    argparser.add_argument("--source", metavar="S", type=int, help="the index of the source to use.")
    args = argparser.parse_args()
    
    sources = [("Training Images",["training_set/*"]),
               ("Camera","")
              ]
    if args.source is None:
      print("Which source would you like predict from?")
      for i, src in enumerate(sources):
        print(f"({i}) {src[0]}")
      src_i = int(input())
    else:
      src_i = args.source
    
    interpreter = tflite.Interpreter("model/model.tflite")
    interpreter.allocate_tensors()
    
    inputs = interpreter.get_input_details()[0];
    outputs = interpreter.get_output_details()[0];
    width  = inputs["shape"][2]
    height = inputs["shape"][1]
    dtype = inputs["dtype"]
    scale, zero = outputs['quantization']
    
    labels = []
    with open("model/labels.txt", "r") as f:
        labels = [line.strip() for line in f.readlines()]
    
    print(f"Predicting from source: {sources[src_i][0]}")

    for img_path in feed(sources[src_i][1]):
      img = Image.open(img_path).convert('RGB').resize((width, height))
    
      # we need another dimension
      input_data = np.expand_dims(img, axis=0)
      # lets get to it
      interpreter.set_tensor(inputs["index"], input_data)
      interpreter.invoke()
    
      output_data = interpreter.get_tensor(outputs["index"]).squeeze()
      output_data = (scale*100) * (output_data - zero)
    
      ordered_indexes = np.flip(output_data.argsort())
      best_index = ordered_indexes[0]
      print(f"  * {img_path} = {labels[best_index]} %0.0f%%" % output_data[best_index])
    
      if best_index == 0:
        unicorn_car()
        scroll_car()
      elif best_index == 1:
        unicorn_person()
        scroll_person()
      else:
        unicorn_nothing()
        scroll_nothing()
      #time.sleep(1)

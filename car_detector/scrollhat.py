#!/usr/local/bin/python3

from gpiozero import Button
import scrollphathd as sp
import numpy as np
import time

def scroll_car():
    car = np.array([ 
    [ 1, 7], [1, 8], [1, 9], [1, 10],
    [2, 6], [2, 11],
    [3, 3], [3, 4], [3, 5], [3, 12], [3, 13], [3, 14],
    [4, 3], [4, 14],
    [5, 3], [5, 4], [5, 5], [5, 6], [5, 7], [5, 8], [5, 9], [5, 10], [5, 11], [5, 12], [5, 13], [5, 14],
    [6, 6], [6, 5], [6, 11], [6, 12] ])
    for c in car:
        c[0] -= 1
    pixelart_show(car)


def scroll_person():
    dude = np.array([
    [1,0], [1,1], [2,0], [3,0], [4, 0], [5, 0], [5, 1],
    [2, 2], [3, 2], [4, 2], [3, 3], [2, 4], [3, 4], [4, 4],
    [1, 5], [0, 6], [5, 5], [6, 6], [3, 5], [3, 6], [3, 7], 
    [2, 7], [2, 8], [2, 9], [2, 10], [2, 11], [2, 12],
    [4, 7], [4, 8], [4, 9], [4, 10], [4, 11], [4, 12],
    [0, 12], [1, 12], [5, 12], [6, 12]
    ])
    for d in dude:
        d[1] += 2
    pixelart_show(dude)


def scroll_nothing():
    nada = np.array([
    [0, 0], [1, 1], [2, 2], [3, 3], [3, 4], [2, 5], [1, 6], [0, 7],
    [6, 0], [5, 1], [4, 2], [4, 5], [5, 6], [6, 7]])

    for n in nada:
        n[1] += 4
    pixelart_show(nada)

def pixelart_show(img):
    sp.clear()
    for c in img:
        sp.set_pixel(c[1], c[0], 0.8)
    sp.show()

def scroll_on():
    sp.clear()
    sp.write_string("ON")
    sp.show()

def scroll_off():
    sp.clear()
    sp.write_string("OFF")
    sp.show()

def scroll_write(string):
    sp.clear()
    sp.write_string(string)
    sp.show()


if __name__ == '__main__':

    button_a = Button(5)
    button_b = Button(6)

    button_a.when_pressed = on
    button_b.when_pressed = off

    x = 0
    while True:
        ## do nothing
        if x%3 == 0:
            pixelart_show(car())
        elif x%3 == 1:
            pixelart_show(person())
        else:
            pixelart_show(nothing())
    
        time.sleep(2)
        x += 1
    


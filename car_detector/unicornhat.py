from unicornhatmini import UnicornHATMini
import numpy as np
import time

uh = UnicornHATMini()

def unicorn_car():
    carpixels = []
    for c in range(5):
        carpixels.append([1,c+6])
    for c in range(7):
        carpixels.append([2,c+5])
    for c in range(11):
        carpixels.append([3,c+3])
        carpixels.append([4,c+3])
    for c in range(2):
        carpixels.append([5, c+3])
    for c in range(3):
        carpixels.append([5,c+7])
    for c in range(2):
        carpixels.append([5,c+12])

    wheels = []
    for w in range(2):
        wheels.append([5, w+5])
        wheels.append([6, w+5])
        wheels.append([5, w+10])
        wheels.append([6, w+10])

    uh.clear()
    for pixels in carpixels:
        uh.set_pixel(pixels[1], pixels[0], 255, 0, 0)
    for pixels in wheels:
        uh.set_pixel(pixels[1], pixels[0], 105, 105, 105)
    uh.show()
    
def unicorn_person():
    personpixels = []
    for p in range(3):
        personpixels.append([0, p+3])
    for p in range(1):
        personpixels.append([1, p+6])
        personpixels.append([1, p+14])
    for p in range(3):
        personpixels.append([2, p+2])
    for p in range(9):
        personpixels.append([2, p+6])
    for p in range(8):
        personpixels.append([3, p+2])
    for p in range(3):
        personpixels.append([4, p+2])
    for p in range(9):
        personpixels.append([4, p+6])
    for p in range(1):
        personpixels.append([5, p+6])
        personpixels.append([5, p+14])
    for p in range(3):
        personpixels.append([6, p+7])

    print("Person going on...")
    uh.clear()
    for pixels in personpixels:
        uh.set_pixel(pixels[1], pixels[0], 0, 0 , 255)
    uh.show()
    
def unicorn_nothing():
    nopepixels = []
    for n in range(1):
        nopepixels.append([0, n+4])
        nopepixels.append([0, n+10])
        nopepixels.append([1, n+5])
        nopepixels.append([1, n+9])
        nopepixels.append([2, n+6])
        nopepixels.append([2, n+8])
        nopepixels.append([3, n+7])
        nopepixels.append([4, n+6])
        nopepixels.append([4, n+8])
        nopepixels.append([5, n+5])
        nopepixels.append([5, n+9])
        nopepixels.append([6, n+4])
        nopepixels.append([6, n+10])

    print("Nothing going on...")
    uh.clear()
    for pixels in nopepixels:
        uh.set_pixel(pixels[1], pixels[0], 255, 0, 0)
    uh.show()


def cars_text():
    frame = [[0,12], [1,12], [2,12], [1,13], [2, 14], [2,13]]
    return frame

if __name__ == '__main__':
    
    x=0
    while True:
        print("In loop")
        if x%3 == 0:
            unicorn_car()
        elif x%3 == 1:
            unicorn_person()
        else:
            unicorn_nothing()
        time.sleep(2)
        x+=1


